const program = require('commander');
const child = require('child_process');
const info = require('./package.json');
const scheduler = require('./src/scheduler');
const job = require('./src/job');
const launchChrome = require('chrome-launch');
const lists = require('./src/lists');
const delay = require('./src/delay');

program
    .version(info.version)
    .option('-f, --force', 'Force close unrelevant Twitch Tabs.')
    .option('-c, --count <n>', 'Number of Streams to keep open.')
    .option('-p, --partner', 'Force only partners in the partners.json list are opend')
    .option('-s, --scheduled [time]', 'Run the Program in Scheduler Mode', 30)
    .option('-g, --google', 'Launch a google instance with required parameters')
    .option('-u, --update')
    .parse(process.argv);


(async () => {

    if (program.update) {
        await lists.update();
        return;
    }
    if (program.google) {
        let instance = launchChrome('chrome://newtab', {
            args: '--remote-debugging-port=9222'
        });

        await delay.delay(10);

        process.on('exit', () => {
            console.log("Shutting Down chrome");
            instance.kill();
        });
    }

    let options = {
        targetTabs: program.count ? program.count : 8,
        force: program.force,
        partner: program.partner,
        count: program.count
    };

    if (program.scheduled) {
        scheduler.run({
            ...options,
            time: program.scheduled
        });
    } else {
        job.run(options);
    }
})();