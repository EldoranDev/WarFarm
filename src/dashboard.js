const info = require('../package.json');
const blessed = require('blessed');

let screen;
let content;
let streamCount;
let streamerBox;
let log;

module.exports.show = function (cmds, options) {
    screen = blessed.screen({
        smartCSR: true
    });

    screen.title = "WarFarm - " + info.version;

    let content = blessed.box({
        width: '100%',
        height: '100%-3',
        padding: 1,
        border: {
            type: 'line'
        }
    });

    content.setLabel(` ${screen.title} `);

    let commands = Object.keys(cmds).map((e) => {
        return `${e}: ${cmds[e].desc}`
    });

    if(options.force) {
        commands.push('{red-fg}None relevant twitch tabs will be closed{/}');
    }

    if(options.partner) {
        commands.push('{yellow-fg}Only known partners will be used{/}');
    }

    streamCount = blessed.progressbar({
        parent: content,
        border: 'line',
        style: {
          fg: 'green',
          bg: 'default',
          bar: {
            bg: 'default',
            fg: 'green'
          },
          border: {
            fg: 'default',
            bg: 'default'
          }
        },
        ch:':',
        left: '10%',
        width: '90%-2',
        height: 3,
    });

    blessed.text({
        parent: content,
        content: 'Streams: ',
        left: 0,
        top: 1,
        width: '10%'
    });

    streamerBox = blessed.text({
        top: 3,
        width: '100%-3',
        height: '30%',
        label: 'Open Streamer',
        parent: content,
        border: 'line'
    })

    log = blessed.log({
        parent: content,
        top: '30%+3',
        label: 'Log',
        width: '100%-3',
        height: '40%',
        border: 'line',
        tags: true,
        keys: true,
        scrollable: true,
    });

    log.enableKeys();

    let comBox = blessed.box({
        width: '100%',
        height: 3,
        top: '100%-3',
        content: commands.join(' | '),
        padding: {
            left: 1,
            right: 1
        },
        tags: true,
        border: {
            type: 'line'
        }
    });

    screen.append(content);
    screen.append(comBox);

    screen.key(Object.keys(cmds), (ch, key) => {
        if (Object.keys(cmds).includes(ch)) {
            cmds[ch].method();
        }
    });

    log.focus();
    screen.on('keypress')

    update();
};

module.exports.setStreamer = function (streamer, percentage) {
    streamCount.setProgress(Math.max(0, Math.min(percentage * 100, 100)));

    streamerBox.setContent(streamer.join(', '));

    update();
}

module.exports.error = function(message) {
    writeLog(message, "ERROR", "red");
}

module.exports.log = function(message) {
    writeLog(message, "LOG", undefined);
};

module.exports.warn = function (message) {
    writeLog(message, "WARN", "yellow");
}

function writeLog(message, prefix, color) {
    let date = new Date();
    
    let ds = date.toLocaleTimeString({
        year: 'numberic',
        day: 'numberic',
        month: 'numeric',
        hour: '2-digit',
        minute: '2-digit'
    });

    if(color) {
        log.add(`{${color}-fg}[${ds}][${prefix}] ${message}{/}`); 
    } else {
        log.add(`[${ds}][${prefix}] ${message}`);
    }
}

function update() {
    screen.render();
}