const parse5 = require('parse5');

module.exports.nodeExists = function (DOM, selector) {
    return new Promise(async (resolve) => {
        let document = await DOM.getDocument();

        DOM.querySelector({
            nodeId: document.root.nodeId,
            selector: selector
        }).then(async (e) => {
            if (e != undefined && e.nodeId != 0) {
                resolve(true);
            } else {
                resolve(false);
            }
        }).catch(() => {
            resolve(false);
        });
    });
}

module.exports.getNodeHTML = function (DOM, selector) {
    return new Promise(async (resolve) => {
        let document = await DOM.getDocument();

        DOM.querySelector({
            nodeId: document.root.nodeId,
            selector: selector
        }).then(async (node) => {
            if(node != undefined && node.nodeId != 0) {
                let result = await DOM.getOuterHTML({nodeId: node.nodeId});
                resolve(result.outerHTML);
            } else {
                resolve("");
            }
        }).catch(() => {
            resolve("");
        });
    });
}

module.exports.getNodes = function (DOM, selector) {
    return new Promise(async (resolve) => {
        const document = await DOM.getDocument();

        
        DOM.querySelectorAll({
            nodeId: document.root.nodeId,
            selector: selector
        }).then(async (result) => {
            let nodes = [];

            for(let i = 0; i < result.nodeIds.length; i++) {
                let text = await DOM.getOuterHTML({nodeId: result.nodeIds[i]});

                nodes.push(parse5.parseFragment(text.outerHTML));
            }

            resolve(nodes);
        }).catch(() => {
            resolve([]);
        });
    });
}