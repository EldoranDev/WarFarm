const path = require('path');
const fs = require('fs')
const GIT = require('nodegit');

const info = require('../package.json');

let partnerList = [];
let blacklistList = [];

const listFolder = path.join(__dirname, '..', 'lists');

module.exports.update = function () {
    return new Promise(async (resolve) => {

        fs.exists(listFolder, async (exists) => {
          
            if(exists) {
                let repo = await GIT.Repository.open(listFolder)
                await repo.fetchAll();
                await repo.mergeBranches('master', 'origin/master');
            } else { 
                await GIT.Clone(info['_repos'].lists, listFolder)
            }
            
            reload();
            resolve();
        });
    });
}

module.exports.isPartner = function (streamer) {

}

module.exports.isBlacklsited = function (streamer) {

}

function reload() {
    const partner = path.join(listFolder, 'partner.json');
    const blacklist = path.join(listFolder, 'blacklist.json');

    partnerList = JSON.parse(fs.readFileSync(partner));
    blacklistList = JSON.parse(fs.readFileSync(blacklist));
}

reload();