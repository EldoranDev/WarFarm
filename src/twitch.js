const CDP = require('chrome-remote-interface');
const parse5 = require('parse5');
const querySelector = require('./querySelector');
const delay = require('./delay').delay;
const browser = require('./browser');

class twitch {

    constructor(client) {
        this.client = client;

        this.DOM = client.DOM;
    }

    isOnline() {
        return new Promise(async (resolve) => {
            const online = await querySelector.nodeExists(this.client.DOM, '.pl-pinned-panel.pl-pinned-panel--animate');

            resolve(!online);
        });
    }

    streamsGame(game) {
        return new Promise(async (resolve) => {
            const html = await querySelector.getNodeHTML(this.client.DOM, 'a[data-a-target="stream-game-link"]');

            resolve(html.includes(game));
        });
    }

    streams() {
        return new Promise(async (resolve) => {
            const hosting = await querySelector.nodeExists(this.client.DOM, 'div[data-a-target="hosting-ui-header"]');

            resolve(!hosting);
        });
    }

    close() {
        this.client.close();
    }
}

module.exports.getAvailableStreams = function (url) {
    return new Promise(async (resolve) => {
        let client = await module.exports.create(url);

        let streamerNodes = await querySelector.getNodes(client.DOM, 'p.tw-thumbnail-card__meta');
        
        let streamer = streamerNodes.map((e) => {
            return e.childNodes[0].childNodes[0].value.split(' ')[0];
        });

        client.close();
        browser.closeTab(client.pageID);
        resolve(streamer);
    });
}

module.exports.create = function (url) {
    return new Promise(async (resolve) => {
        let target = await CDP.New();
        CDP({ target }).then(async (client) => {
            const { Page, DOM } = client;

            await Promise.all([Page.enable(), DOM.enable()]);

            await Page.navigate({url: url});
            await Page.loadEventFired();

            await delay(5);

            client['pageID'] = target.id;

            resolve(client);
        }).catch((e) => {
            console.error(e);
            resolve(undefined);
        })
    });
};

module.exports.connect = function (tab) {
    return new Promise((resolve) => {
        CDP({ target: tab.webSocketDebuggerUrl }).then(async client => {

            const { DOM, Page } = client;

            await Promise.all([
                DOM.enable(),
                Page.enable(),
            ]);

            resolve(new twitch(client));

        }).catch(() => {
            resolve(undefined);
        });
    });
}

