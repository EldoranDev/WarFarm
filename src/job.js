const fetch = require('node-fetch');
const CDP = require('chrome-remote-interface');
const delay = require('./delay');
const parse = require('parse5');

const browser = require('./browser');
const twitch = require('./twitch');
const partnerList = require('../partner.json');

const HUB_URL = 'https://go.twitch.tv/directory/game/Warframe';

const logger = require('./logger');

module.exports.run = function(options) {

    return new Promise(async (resolve, reject) => {
        const response = await (await fetch('http://localhost:9222/json')).json();
        
            const targetTabs = options.targetTabs;
        
            let tabs = await browser.getOpenTabs((e) => {
                return e.url.startsWith('https://go.twitch.tv/');
            });
        
            logger.log("Number of Twitch Tabs: " + tabs.length);
        
            for (let i = 0; i < tabs.length; i++) {
        
                let shouldClose = false;
        
                /**
                 * @var {twitch} client
                 */
                let client = await twitch.connect(tabs[i]);
        
                if (client != undefined) {
                    if (await client.isOnline()) {
        
                        const checks = await Promise.all([
                            client.streams(),               // Check if not hosting
                            client.streamsGame('Warframe'), // check if game is warframe
                        ]);
        
                        shouldClose = !(checks.reduce((acc, cur) => {
                            return acc && cur;
                        }, true));
                    } else {
                        shouldClose = true;
                    }
                } else {
                    shouldClose = true;
                }
        
                if (shouldClose) {
                    // Close when in force mode
                    logger.log(`Tab (${tabs[i].title}) is not Streaming Warframe`);
        
                    if (options.force) {
                        logger.warn("Tab will be closed because of force mode (-f/--force).");
                        await browser.closeTab(tabs[i].id);
                    }
                }
        
                client.close();
            }
        
            let openTabs = await browser.getOpenTabs((e) => e.url.startsWith('https://go.twitch.tv/') && e.url !== HUB_URL);
        
            let openStreams = openTabs.map((e) => {
                return e.title.split(' - ')[0];
            });
                
            if (openTabs.length < targetTabs) {
                let newTabs = 0;
        
                let streams = await twitch.getAvailableStreams(HUB_URL)
        
                streams = streams.filter((e) => {
                    return options.partner ? partnerList.includes(e) : true; // Check if Partner ?
                });
        
                logger.log(`${targetTabs - openTabs.length} new tabs will be opend`);
        
                for (let i = 0; i < streams.length && newTabs + openTabs.length < targetTabs; i++) {
                    if (!openStreams.includes(streams[i])) {
                        let stream = await twitch.create('https://go.twitch.tv/' + streams[i]);
                        stream.close();
                        newTabs++;
                        openStreams.push(streams[i]);
                    }
                }
            }

            logger.log("Finished run.");
            resolve(openStreams);
    });
}