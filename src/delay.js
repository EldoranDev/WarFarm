module.exports.delay =  function (seconds) {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res();
        }, seconds*1000);
    });
}