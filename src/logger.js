let logHandler = (m) => {
    console.log(m);
}

let errorHandler = (e) => {
    console.error(e);
}

let warnHandler = (w) => {
    console.warn(w);
}

module.exports.log = function(m) {
    logHandler(m);
}

module.exports.error = function(e) {
    errorHandler(e);
}

module.exports.warn = function(w) {
    warnHandler(w);
}

module.exports.init = function(message, warn, error) {
    logHandler = message;
    warnHandler = warn;
    errorHandler = error;
}