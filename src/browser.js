const fetch = require('node-fetch');

module.exports.closeTab = function (id) {
    return fetch('http://localhost:9222/json/close/' + id);
}

module.exports.getOpenTabs = function (f) {
    return new Promise(async (resolve) => {
        const tabs = await(await fetch('http://localhost:9222/json')).json();

        let t = tabs.filter((e) => f(e));
        
        resolve(t);
    });
}

module.exports.getNumberOfOpenTabs = function (filter) {
    return new Promise(async (resolve) => {
        const tabs = await module.exports.getOpenTabs(filter);

        resolve(tabs.length);
    });
}