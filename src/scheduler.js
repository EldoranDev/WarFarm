const dashboard = require('./dashboard');
const logger = require('./logger');
const job = require('./job');
const lists = require('./lists');

module.exports.run = function (options) {

    let commands = {
        q: {
            desc: 'quit the application',
            method: () => {
                process.exit(0);
            }
        },/*
        r: {
            desc: 'restart the application',
            method: () => {

            }
        },
        */
        f: {
            desc: 'force streamer refresh',
            method: () => {
                run(options);
            }
        }
    };

    logger.init(
        (m) => {
            dashboard.log(m);
        },
        (w) => {
            dashboard.warn(w);
        },
        (e) => {
            dashboard.error(e);
        }
    )

    run(options);

    setInterval(async () => {
        await lists.update();
        run(options);
    }, options.time * 60 * 1000);

    dashboard.show(commands, options);
}

function run (options) {
    job.run(options).then((streamer) => {
        dashboard.setStreamer(streamer, streamer.length/options.targetTabs);
    }).catch((e) => {
        dashboard.error("Error while running the job.");
        dashboard.error(e);
    });
}