# WARFARM 

## How to use

Linux: ```apt-get update && apt-get install git```  
Windows: Download and install from [Git](https://git-scm.com)

Install lates version of [NodeJS](https://nodejs.org)

Checkout this Repository: ```git clone https://gitlab.com/EldoranDev/WarFarm.git```

Change into folder and install dependecys (requires nodejs in path):

```
cd WarFarm
npm install
```

run the scheduler (will run the script every 30min)

```npm run scheduled```

alternativly run the script once

```npm run start```

## Parameters

After the npm run <command> you need to write ```--``` to append the arguments.

```-f/--force``` close all twitch tabs that are not currently streaming Warframe

```-c/--count <nr>``` number of twitch tabs you want to keep open

```-p/--partner``` (experimental): Only open Partner Streams

Sample: ```npm run scheduled -- -f -c 10```

## Updating

To update:

1. Run ```git pull```
2. Run ```npm install```